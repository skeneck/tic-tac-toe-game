import React from 'react';
// import { Formik, Form, } from 'formik';
import './Form.css';

export default class Game extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = {
			etat_btn: false,
			error: '',
			form: {
				player1: '',
				player2: ''
			}
		};

		this.input1 = React.createRef();
		this.input2 = React.createRef();

		this.checkFields = this.checkFields.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	validatePlayer() {
		if (this.input1.current.value && this.input2.current.value) {
			if (this.input1.current.value.toUpperCase() === this.input2.current.value.toUpperCase()) {
				this.setState({
					error: 'Ce nom de joueur est déjà choisi !'
				});

				return false
			}
			
			return true
		}

		return false
	}

	checkFields() {
		if (this.state.error) {
			this.setState({
				error: ''
			});
		}

		if (this.validatePlayer()) {
			this.setState({
				form: {
					player1: this.input1.current.value,
					player2: this.input2.current.value
				},
				etat_btn: true
			});
		} else {
			this.setState({
				form: {
					player1: '',
					player2: ''
				},
				etat_btn: false
			});

			return;
		}
	}

	handleSubmit(event) {
		event.preventDefault();
		this.props.onClick(this.state.form)
	}

	render() {
		return (
			<form className='form' onSubmit={ this.handleSubmit } >
				<div className='container-error'>{ this.state.error }</div>

				<label>
					Nom Joueur 1
					<input
						ref={ this.input1 }
						className='field-player1'
						placeholder='Bob'
						type="text"
						name="player1"
						onChange={ this.checkFields }
					/>
				</label>
		
				<label>
					Nom Joueur 2
					<input
						ref={ this.input2 }
						className='field-player1'
						placeholder='Tom'
						type="text"
						name="player2"
						onChange={ this.checkFields }
					/>
				</label>
		
				<button className= { this.state.etat_btn ? 'btn-start-form' : 'btn-start-game-disabled' } type="submit">Commencer</button>
			</form>
		);
	}
}
