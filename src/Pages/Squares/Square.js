import './Square.css';

export default function Square(props) {
	return (
		<button className={ ((props.value==='X') ? 'color-square ' : '') + "square"} onClick={ props.onClick }>
			{ props.value }
		</button>
	);
}
