import Game from './Games/Game';

export default function App() {
	return (
		<div>
			<Game />
		</div>
	);
}
