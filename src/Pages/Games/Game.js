import React from 'react';
import Board from '../Boards/Board';
import Form from '../Forms/Form';
import './Game.css';

export default class Game extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = {
			gameStart: false,
			player: {
				X: '',
				O: ''
			},
			history: [
				{
					squares: Array(9).fill(null)
				}
			],
			stepNumber: 0,
			xIsNext: true
		};
	}

	calculateWinner(squares) {
		const lines = [
			[0, 1, 2],
			[3, 4, 5],
			[6, 7, 8],
			[0, 3, 6],
			[1, 4, 7],
			[2, 5, 8],
			[0, 4, 8],
			[2, 4, 6]
		];
		
		for (let i = 0; i < lines.length; i++) {
			const [a, b, c] = lines[i];
			
			if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
				return squares[a];
			}
		}
	
		return null;
	}

	handleClick(i) {
		const history = this.state.history.slice(0, this.state.stepNumber + 1);
		const current = history[history.length - 1];
		const squares = current.squares.slice();
		
		if (this.calculateWinner(squares) || squares[i]) {
			squares[i] = this.state.xIsNext ? "X" : "O";
			
			return;
		}
		
		squares[i] = this.state.xIsNext ? "X" : "O";
		
		this.setState({
			history: history.concat([
				{
					squares: squares
				}
			]),
			stepNumber: history.length,
			xIsNext: !this.state.xIsNext
		});
	}

	handleFormClick(values) {
		if (Object.keys(values).length === 2) {
			this.setState({
				player: {
					X: values.player1,
					O: values.player2
				},
				gameStart: true,
			});
		}
	}

	jumpTo(step) {
		this.setState({
			stepNumber: step,
			xIsNext: (step % 2) === 0,
			history: [
				{
					squares: Array(9).fill(null)
				}
			],
		});
	}

	reloadPage() {
		window.location.reload()
	}

	render() {
		const history = this.state.history;
		const current = history[this.state.stepNumber];
		const winner = this.calculateWinner(current.squares);

		const moves = history.map((step, move) => {
			const desc = move ? 'Go to move #' + move : 'Rejouer';
			
			return (
				<li className={ (desc === 'Rejouer') ? 'container-btn' : 'hidden-btn' } key={move}>
					<button className={ (this.state.history.length > 1) ? 'btn-start-game' : 'btn-start-game-disabled' } onClick={ () => this.jumpTo(move) }>{ desc }</button>
				</li>
			);
		});

		let status;
		
		if (winner) {
			status = "Bravo !  " + ((winner==='X')?this.state.player.X.toUpperCase() : this.state.player.O.toUpperCase()) + " a gagné.";
			// status = "Winner: " + winner;
		} else {
			status = this.state.xIsNext ? this.state.player.X : this.state.player.O;
		}

		return (
			<div className='container-game'>
				<div className='game-title'>
					<h1>Tic-Tac-Toe Game</h1>
				</div>

				<div className={ this.state.gameStart ? 'hidden-form' : '' } >
					<Form onClick={ (values) => this.handleFormClick(values) } />
				</div>

				<div className={ this.state.gameStart ?'' : 'hidden-game' } >
					<div className="game-info">
						<div className='container-status'>
							<span className={ ((status!==this.state.player.X) && (status!==this.state.player.O)) ? 'indice' : '' }>Joueur suivant: </span>
							<span className={ 'status' + ( ((status!==this.state.player.X) && (status!==this.state.player.O)) ? ' status-color' : '' ) }>{ status }</span>
						</div>
						<ol>{ moves }</ol>
						<button className='btn-start-game btn-stop-game' onClick={ () => this.reloadPage() }>Arrêter</button>
					</div>

					<div className="game">
						<div className="game-board">
							<Board
								squares={ current.squares }
								onClick={ (i) => this.handleClick(i) }
							/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
